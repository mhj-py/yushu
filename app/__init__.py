from flask import Flask

# 导入创建的数据库
from app.models.book import db


def creat_app():
    app = Flask(__name__)
    app.config.from_object('app.setting')
    app.config.from_object('app.secure')
    registe_blueprint(app)

    # 把SQLALchemy对象插入到falsk核心对象上
    db.init_app(app)
    #根据模型生成数据库表结构
    with app.app_context():
        db.create_all(app=app)
    return app


def registe_blueprint(app):
    from app.web.book import web
    app.register_blueprint(web)
