from sqlalchemy import Column,Integer,String
from flask_sqlalchemy import SQLAlchemy

#实例化数据库
db=SQLAlchemy()

class Book(db.Model):
    #字段信息
    id=Column(Integer,primary_key=True,autoincrement=True)
    #string(字符的长度)，nullable,能否为空
    title=Column(String(50),nullable=False)
    #default,，默认值
    author=Column(String(30),default='未知')