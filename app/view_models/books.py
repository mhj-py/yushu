"""数据要求"""

# {
#     #书本列表
#     'book':[
#         {
#             'title':'',
#             'publisher':'',
#             'pages':'',
#             'auther':'',
#             'price':0,
#             'summary':'',
#             'image':''
#         }
#     ],
#     'total':0,#总书本书
#     'keyword':'',#搜索关键字，方便前端
# }

class BookVieModel:
    @classmethod
    #处理单本书isbn号单本书或没有书的结果
    def package_single(cls,data,keyword):
        returned={
            'books':[],
            'total': 0,
            'keyword': keyword,
        }
        if data:
            returned['total']=1
            returned['books'].append(cls.handle_book_data(data))
        return returned

    @classmethod
    # 处理搜索关键字的结果
    def package_collection(cls, data, keyword):
        returned={
            'books':[],
            'total':0,
            'keyword':keyword,
        }
        if data:
            returned['total']=data['total']
            returned['books']=[cls.handle_book_data(
                book_iteam) for book_iteam in data['books']]
        return returned

    @classmethod
    #书本数据格式化
    def handle_book_data(cls,data):
        #把data变成字典
        book={
            'title':data['title'],
            'publisher':data['publisher'],
            'pages':data['pages'],
            'auther':'、'.join(data['author']),
            'price':data['price'],
            'summary':data['summary'],
            'image':data['image']
        }
        return book