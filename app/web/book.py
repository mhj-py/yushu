from flask import jsonify, request

from app.forms.book import SearchForm
from app.lib.helper import is_isbn_or_key
from app.spider.yushu_book import YuShuBook
from app.view_models.books import BookVieModel
from . import web

@web.route('/test')
def test1():
    from app.lib.none_local import n
    print(n.v)
    n.v=2
    print(20*'.')
    print(getattr(request,'v',None))
    setattr(request,'v',2)
    print(20*'.')
    return 'test'

@web.route('/book/search')
def search():
    '''
        q:普通关键字, isbn
        page:页码
        /book/search?q=9787806737842&page=1
    '''
    # Request
    # HTTP请求信息：请求方式、参数、remote ip等等，可以看成对WSGI中environ的封装
    # q = request.args['q']
    # print(q)
    # page = request.args['page']
    # print(page)
    # d = request.args.to_dict()
    form=SearchForm(request.args)
    #怎么样判断数据是否合法
    if form.validate():
        q=form.q.data
        page=form.page.data
        isbn_or_key = is_isbn_or_key(q)
        if isbn_or_key == 'isbn':
            data = YuShuBook.search_by_isbn(q)
            result=BookVieModel.package_single(data,q)
        else:
            data = YuShuBook.search_by_keyword(q)
            result=BookVieModel.package_collection(data,q)
        return jsonify(result)
    return jsonify(form.errors)
