from flask import current_app
from app.lib.httper import HTTP


class YuShuBook:
    isbn_url = 'https://api.douban.com/v2/book/isbn/{}'
    keyword_url = 'https://api.douban.com/v2/book/search?q={}&start={}&count={}'

    @classmethod
    def search_by_isbn(cls, q):
        url = cls.isbn_url.format(q)
        return HTTP.get(url)

    @classmethod
    def search_by_keyword(cls, q, page=1):
        url = cls.keyword_url.format(q,cls.calculate_start(page), current_app.config['PER_PAGE'])
        return HTTP.get(url)

    @staticmethod
    def calculate_start(page):
        #通过页码计算开始记录
        return (page-1)*current_app.config['PER_PAGE']
if __name__ == '__main__':
    print(YuShuBook.search_by_isbn('9787806737842'))
