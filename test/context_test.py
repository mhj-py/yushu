from flask import Flask,current_app

app=Flask(__name__)

#应用上下文  对象 flask
#请求上下文  对象  request
#Flask AppContext
#Request RequestContext

ctx=app.app_context()
ctx.push()

# a=current_app
#
# d=a.config['DEBUG']
# print(d)

#RuntimeError: Working outside of application context.

#我们通过crrent_app得到核心对象，应该从上下文从去取
#单元测试里面需要用到
with app.app_context():
    a=current_app
    d=a.config['DEBUG']
    print(d)