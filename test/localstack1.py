import time

from werkzeug.local import LocalStack
from threading import Thread
my_stack = LocalStack()
my_stack.push(1)
print(f'在主线程中，栈顶的结果是{my_stack.top}')


def work():
    #新线程
    print(f'在新线程中，栈顶的结果是{my_stack.top}')
    my_stack.push(2)
    print(f'在新线程中，栈顶的结果是{my_stack.top}')

newt=Thread(target=work)
newt.start()
time.sleep(1)
print(f'最后在主线程中，栈顶的结果是{my_stack.top}')
