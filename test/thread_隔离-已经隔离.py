import threading
import time

from werkzeug.local import Local



my_odj=Local()
my_odj.b=1

print(f'in main thread b is {my_odj.b}')


def worker():
    my_odj.b = 2
    print(f'in new thread b is {my_odj.b}')


new_t = threading.Thread(target=worker)

new_t.start()
time.sleep(1)
print(f'in main thread b is {my_odj.b}')
# 线程共享同一进程的数据
