import threading
import time

def hello():
    print("I'am the new thread")
    t=threading.current_thread()
    time.sleep(1)
    print(t.getName())

new_t=threading.Thread(target=hello,name='ten thread')
new_t.start()

t=threading.current_thread()
print(t.getName())

#线程更加充分的利用cpu的优势
#异步编程