from flask import Flask
from flask import Blueprint

app = Flask(__name__)

web=Blueprint('web',__name__)

@web.route('/hello')
def hello():
    return 'hello'

app.register_blueprint(web)#把蓝图接入app


if __name__ == '__main__':
    # app.run('0.0.0.0',debug=app.config['DEBUG'],port=81)
    app.run()
