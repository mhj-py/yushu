from threading import Thread, Lock

money=0
lock=Lock()
def changemoney(num):
    global money
    try:
        #先加锁
        lock.acquire()
        for i in range(num):
            money+=num
            money-=num
    except:
        pass
    finally:
        #释放锁
        lock.release()


thr1=Thread(target=changemoney,args=(9999999,))
thr2=Thread(target=changemoney,args=(9999999,))
thr1.start()
thr2.start()
thr1.join()
thr2.join()

print(money)