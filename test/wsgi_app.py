def application(environ, start_response):
    path=environ.get('PATH_INFO')
    # start_response('200 OK', [('Content-Type', 'text/html')])
    # return [b'<h1>Hello, web!</h1>']
    if path=='/':
        start_response('200 OK', [('Content-Type', 'text/html')])
        return [b'<h1>Hello, web!</h1>']
    else:
        start_response('200 OK', [('Content-Type', 'text/html')])
        return [path.lstrip('/').encode('utf-8')]